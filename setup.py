from setuptools import setup

setup(
    name="sourcehook",
    version="0.0.4",
    author="Borodin Gregory",
    author_email="grihabor@gmail.com",
    url="https://github.com/grihabor/sourcehook",
    package_dir={"": "src"},
    py_modules=["sourcehook"],
)
