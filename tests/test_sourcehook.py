import pytest
from sourcehook import _maybe_modify_source


@pytest.mark.parametrize(
    "data, expected",
    [
        (b"abc", b"abc"),
        (b"abc\ndef", b"abc\ndef"),
        (
            b"# sourcehook: enable\na = 1",
            b"\n".join(
                [
                    b"def _global_scope():",
                    b"    a = 1",
                    b"    return locals()",
                    b"",
                    b"globals().update(_global_scope())",
                ]
            ),
        ),
    ],
)
def test_maybe_modify_source(data, expected):
    assert expected == _maybe_modify_source(data)
